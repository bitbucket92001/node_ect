// モジュールをロード
var http = require('http');
var socketio = require('socket.io');
var express = require('express');
var router = express();
var server = http.createServer(router);
var io = socketio.listen(server);

//Expressのテンプレートについてくるので
//一応ロードする。
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var ECT = require('ect'); //add ect
var ectRenderer = ECT({
    watch: true,
    root: __dirname + '/views',
    ext: '.ect'
}); //add ect
var routes = require('./routes/index');
var users = require('./routes/users');

// view engine setup
router.engine('ect', ectRenderer.render); //modified ect
router.set('view engine', 'ect'); //modified ect

//path.join()は引数に指定されたファイルを"/"で区切りパス情報にする
//faviconが決まったら、public直下に「favicon.ico」という名でおいてコメントを外す
//router.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
router.set('views', path.join(__dirname, 'views'));
router.use(logger('dev'));
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
    extended: false
}));
router.use(cookieParser());

//public直下のリソースを使用する際に、
//サーバー側、クライアント側の両方の記述が楽になる
router.use(express.static(path.join(__dirname, 'public')));
router.use('/', routes);
router.use('/users', users);

//-------------HTMLをそのまま送るとき-------------
//router.get('/', function(req, res,next) { 
//    res.sendFile(__dirname + '/views/client.html');
//});


//--------------Error Handler-------------
// development error handler
// will print stacktrace
if (router.get('env') === 'development') {
    router.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

//------ production error handler-----
// no stacktraces leaked to user
router.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});
//---------------------------------------

// server.listen(process.env.PORT || 4200);

server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function(){
  var addr = server.address();
  console.log("server listening at", addr.address + ":" + addr.port);
});