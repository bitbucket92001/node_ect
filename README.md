モジュールのインストール

    以下のコマンドを実行するとpackage.jsonに従って必要なnode_modulesがインストールされる
    npm install
    エラーが出るけれど動くようだ

実行方法

    app.jsを含むフォルダーでコマンドプロントから
    「 gulp 」
    と打てば、ファイルの変更があればサーバーの再起動とブラウザの更新をしてくれる
    ※ gulpを使う前に 「 npm install gulp -g 」 でインストールしなければ使えないかも

    ページは http://localhost:4500/

参考

    http://tips.hecomi.com/entry/20131116/1384598882
    ↑ここを見てNode.jsとSocket.ioが使えそうだと思い立つ
        
    http://www.programwitherik.com/socket-io-tutorial-with-node-js-and-express/
    ↑ベースとなるコードを頂戴したサイト
    ExpressとSocket.ioを共存させるのに苦戦したが、このサイトを見て動くようになった
        var server = require('http').createServer(app);  
        var io = require('socket.io')(server);
    確か、上記の2文が重要だった気がする。
    古い記事のコードをそのまま実行するとサーバーのインスタンスが何何とエラーを吐く
    
    https://gist.github.com/sogko/b53d33d4f3b40d3b4b2e
    ↑Gulpのスクリプトはここから頂戴した
